import argparse
import logging
import re
import os
import subprocess

def git_version():
    ''' attempts to return a version number from a git tag '''
    logging.debug('attempting to find git tag based version number')
    major_re = re.compile('(?<=v)\d+(?=.)')
    minor_re = re.compile('(?<=\.)\d+')
    prere_re = re.compile('(?<=\d)(a|b|rc)(?=\-)')
    patch_re = re.compile('(?<=\-)\d+(?=\-)')

    current_dir = os.getcwd()
    os.chdir(os.path.dirname(__file__))

    try:
        git_version = subprocess.check_output(['git', 'describe']).decode()
    except:
        return None

    os.chdir(current_dir)

    version_major = str(major_re.search(git_version).group(0))
    version_minor = str(minor_re.search(git_version).group(0))

    stage_indictor = prere_re.search(git_version)
    if stage_indictor is None:
        version_stage = '.'
    else:
        version_stage = str(stage_indictor.group(0))

    if patch_re.search(git_version) is None:
        version_patch = '0'
    else:
        version_patch = str(patch_re.search(git_version).group(0))

    return f'{version_major}.{version_minor}{version_stage}{version_patch}'

def file_version():
    ''' attempts to return a version number of a version.py file '''
    logging.debug('attempting to find txt file based version number')
    try:
        with open('version.txt', 'r') as fh1:
            return fh1.readline()
    except:
        return None

# set __version__ to a value
git_version = git_version()
file_version = file_version()

if git_version is not None:
    logging.debug('version number set from git tags')
    __version__ = git_version
elif file_version is not None:
    logging.debug('version number set from version.txt file')
    __version__ = file_version
else:
    # A default value for when the proper version can't be found
    # this is a deliberately low value so it doesn't get used by pip
    # but is a valid value so setuptools can install it test environments
    logging.debug('version number set to default value') 
    __version__ = '0.0dev1'


if __name__ == '__main__':
    script_dir = os.path.abspath(os.path.dirname(__file__))
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file_output', action='store_true')
    args = parser.parse_args()

    version = git_version()
    print(version)

    versiontxt_location = os.path.join(script_dir, 'version.txt')
    if args.file_output:
        with open(versiontxt_location, 'w') as fh1:
            fh1.write(version)
