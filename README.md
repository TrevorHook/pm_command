# pm_command

A project manager based on local files and using a command line interface

## Requirements

1. Python3

## Installation

1. download all project files to a local drive
2. create and activate a virtualenv
2. run pip install . into the virtualenv

## Running

1. running *pm --help* will show the available commands.

## Concepts

1. There is a basefolder in which all projects are kept
2. There are 2 folders below the basefolder, *active* and *archived*
3. Upon creating a new project, a folder and empty .org file are placed in the *active* folder.
4. Upon closing a project, the project folder is archived as a zip file under the *acrhived* folder and removed from the *active* folders.
5. I use the project folder to store all documents I generate while working on the projects.
6. folders called *tmp* will not be included in the archive file when the project is closed, useful for keep files which are needed while the project is being worked on but don't need to be stored once the project is closed.

## Advanced Concepts

### Repeating projects

Some projects are ongoing discrete tasks, such as repeating meetings which may generate actions to be worked on.

To handle this scenario, you can add a projectname suffix when creating a project, in the form of *project.suffix*. The only affect the suffix has is when closing the project, the suffix is ignored for the name of the project.zip file name but the full project name and suffix is used as the path of the files and directories added to the archive.

So, by using a common project name with appropriate suffix all notes related projects are archived in the same archive file.
