.PHONY: sync update clean test coverage setup_devenv setup_prodenv setup_testenv lint

testenv := testenv
devenv := devenv
prodenv := prodenv
ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    detected_OS := Windows
		sys_python := python
		devenv_python := $(devenv)\Scripts\python
		testenv_python := $(testenv)\Scripts\python
		prodenv_python := $(prodenv)\Scripts\python
else
    detected_OS := $(shell uname)  # same as "uname -s"
		sys_python := python3
		devenv_python := $(devenv)testenv\bin\python3
		testenv_python := $(testenv)testenv\bin\python3
		prodenv_python := $(prodenv)testenv\bin\python3
endif

default: test

setup_devenv:
	$(sys_python)  -m venv $(devenv)
	$(devenv_python) -m pip install --upgrade pip
	$(devenv_python) -m pip install -e .[dev]

setup_prodenv:
	$(sys_python) -m venv $(prodenv)
	$(prodenv_python) -m pip install --upgrade pip
	$(prodenv_python) -m pip install .

setup_testenv:
	$(sys_python) -m venv $(testenv)
	$(testenv_python) -m pip install --upgrade pip
	$(testenv_python) -m pip install -e .[test]

uninstall:
	$(testenv_python) -m pip uninstall traceability --yes

test: setup_testenv
	$(testenv_python) -m pytest

test_quick: setup_testenv
	$(testenv_python) -m pytest -m "not slow"

coverage: setup_testenv
	$(testenv_python) -m coverage run --omit */site-packages/*,*/test/* -m pytest
	$(testenv_python) -m coverage html

lint: test
	$(testenv_python) -m pylint --ignore=tests project_manager
