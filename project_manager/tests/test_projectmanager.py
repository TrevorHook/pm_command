''' test the projectmanager class '''

# import supporting packages
from datetime import datetime
import logging
from pathlib import Path
import pytest
from pytest_mock import mocker
import re
import shutil
import zipfile

# import packages under test
from ..project import Project, ProjectManager, ProjectExistsError

PROJECT_FOLDER = 'project'
SCENARIOS = 'project_manager/tests/scenarios'

def copyscenario(scenario_name, destination):
    ''' copies the scenario to the destination folder '''
    scenario_folder = SCENARIOS.split('/')
    src_folder = Path(*scenario_folder).joinpath(scenario_name)
    shutil.copytree(src_folder, destination)

def setup_project(basefolder):
    ''' creates an empty project base folder '''
    required_folders = ['active','archived']
    for required_folder in required_folders:
        folder = basefolder.joinpath(required_folder)
        folder.mkdir(parents=True, exist_ok=True)

def test_config_file(tmpdir):
    ''' tests the return of the config_file location '''
    expresult = Path().home().joinpath('.projectmanager.ini')
    actresult = ProjectManager.config_file()

    assert actresult == expresult

@pytest.mark.dependency(name='init')
@pytest.mark.parametrize('test_input', [
    (PROJECT_FOLDER)
    ])
def test_projectmanager_init(tmpdir, test_input):
    ''' test the projectmanager initialization '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    setup_project(basefolder)
    folders = test_input.split('/')
    project_folder = Path(tmpdir).joinpath(*folders)

    pm = ProjectManager(basefolder=str(project_folder))

    expresult = project_folder.resolve()
    actresult = pm.basefolder

    assert actresult == expresult

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('test_input', [
    (PROJECT_FOLDER)
    ])
def test_projectmanager_active_folder(tmpdir, test_input):
    ''' check active folder property '''
    folders = test_input.split('/')
    project_folder = Path(tmpdir).joinpath(*folders)
    setup_project(project_folder)

    pm = ProjectManager(basefolder=str(project_folder))

    expresult = project_folder.joinpath('active').resolve()
    actresult = pm.active_folder

    assert actresult == expresult

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('test_input', [
    (PROJECT_FOLDER)
    ])
def test_projectmanager_archived_folder(tmpdir, test_input):
    ''' check active folder property '''
    folders = test_input.split('/')
    project_folder = Path(tmpdir).joinpath(*folders)
    setup_project(project_folder)

    pm = ProjectManager(basefolder=str(project_folder))

    expresult = project_folder.joinpath('archived').resolve()
    actresult = pm.archived_folder

    assert actresult == expresult

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('test_input,test_output', [
    # single active project
    ('simple', ['SINGLE']),
    # multiple active projects
    ('multiple', ['P1', 'P2']),
    ])
def test__find_open_projects(test_input, test_output, tmpdir):
    ''' checks active projects can be found '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)

    copyscenario(test_input, basefolder)

    pm = ProjectManager(basefolder=str(basefolder))

    expresult = []
    for project in test_output:
        expresult.append(basefolder.joinpath('active', project))

    actresult = [x.path for x in pm._find_open_projects()]

    assert set(actresult) == set(expresult)

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('test_input,test_output', [
    # single active project
    ('simple', ['SINGLE_ARCHIVED.zip']),
    # multiple active projects
    ('multiple', ['A1.zip', 'A2.zip']),
    ])
def test__find_archived_projects(test_input, test_output, tmpdir):
    ''' checks active projects can be found '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)

    copyscenario(test_input, basefolder)

    pm = ProjectManager(basefolder=str(basefolder))

    expresult = []
    for project in test_output:
        expresult.append(basefolder.joinpath('archived', project))

    actresult = pm._find_archived_projects()

    assert set(actresult) == set(expresult)

@pytest.mark.dependency(depends=['init'], name='create')
@pytest.mark.parametrize('test_input, test_output', [
    ('newproject', 'NEWPROJECT'),
    ('new project', 'NEW_PROJECT'),
    ('NEWPROJECT', 'NEWPROJECT')
    ])
def test_create(tmpdir, test_input, test_output, caplog, mocker):
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    setup_project(basefolder)
    caplog.set_level(logging.DEBUG)

    project_mock = mocker.patch.object(Project, 'create')

    pm = ProjectManager(basefolder=str(basefolder))
    pm.create(test_input)

    assert project_mock.called_once()

@pytest.mark.dependency(depends=['create'])
@pytest.mark.parametrize('test_input, test_output',[
    ('abstract', 'abstract')
])
def test_create_abstract(tmpdir, test_input, test_output, mocker):
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    setup_project(basefolder)
    project_mock = mocker.patch.object(Project, 'create')

    pm = ProjectManager(basefolder=str(basefolder))
    pm.create('test', abstract=test_input)

    project_mock.assert_called_with(abstract=test_input)

@pytest.mark.dependency(depends=['create'])
def test_create_existing_project(tmpdir, caplog, mocker):
    ''' checks you can't create the same project twice '''
    # test setup
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    setup_project(basefolder)
    caplog.set_level(logging.DEBUG)

    mocked = mocker.patch.object(
        Project, 'create', side_effect=ProjectExistsError())

    pm = ProjectManager(basefolder=str(basefolder))

    # test verification
    with pytest.raises(ProjectExistsError):
        pm.create('test')

    explog = "Attempted to create TEST but it already exists"

    assert caplog.records[-1].message == explog
    assert caplog.records[-1].levelno == logging.WARNING

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('scenario,test_input,archivename,archivecontents', [
    # single active project
    ('simple', 'single', 'SINGLE.zip',
        ['SINGLE/single.org', 'SINGLE/dir1/dir2/file1.txt']),
    ('ongoing', 'ongoing.20200518', 'ONGOING.zip',
        ['ONGOING.20200511/ongoing.20200511.org',
        'ONGOING.20200518/ongoing.20200518.org']),
    ])
def test_close(tmpdir, scenario, test_input, archivename, archivecontents,
    caplog, mocker):
    ''' check close can archive a project '''
    caplog.set_level(logging.DEBUG)
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    project_mock = mocker.patch.object(Project, 'close')

    copyscenario(scenario, basefolder)

    pm = ProjectManager(basefolder=str(basefolder))
    pm.close(test_input)

    project_mock.assert_called_once()

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('scenario,test_output', [
    # single active project
    ('simple', ['single']),
    ])
def test_list(tmpdir, scenario, test_output):
    ''' check close can archive a project '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)

    copyscenario(scenario, basefolder)

    pm = ProjectManager(basefolder=str(basefolder))
    actresult = pm.list()

    expresult = {}
    for project in test_output:
        name = project.upper()
        expresult[name] = {'path': basefolder.joinpath('active', name)}


    assert actresult == expresult

def test_workjournal_file(tmpdir):
    ''' checks the workjournal_file location '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    setup_project(basefolder)
    expresult = basefolder.joinpath('work journal.md')

    pm = ProjectManager(basefolder=str(basefolder))

    actresult = pm.workjournal_file

    assert actresult == expresult

@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('test_input,test_output',[
    ("a short message", 'a short message')
])
def test_workjournal_entry(tmpdir, test_input, test_output):
    ''' checks adding a new work journal note '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    setup_project(basefolder)
    message_date = datetime.utcnow()
    message_date_str = message_date.strftime('%Y-%m-%dT%H:%MZ')

    expresult = f'\n\n## {message_date_str}\n\na short message'

    pm = ProjectManager(basefolder=str(basefolder))
    pm.workjournal_entry(test_input, message_date)

    with open(basefolder.joinpath('work journal.md'), 'r') as fh1:
        actresult = fh1.read()

    assert actresult == expresult
