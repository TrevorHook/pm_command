''' test the projectmanager class '''

# import supporting packages
from datetime import datetime
import logging
import glob
from pathlib import Path
import pytest
import re
import shutil
import zipfile

# import packages under test
from ..project import Project, ProjectManager, ProjectExistsError

PROJECT_FOLDER = 'project'
SCENARIOS = 'project_manager/tests/scenarios'

def copyscenario(scenario_name, destination):
    ''' copies the scenario to the destination folder '''
    scenario_folder = SCENARIOS.split('/')
    src_folder = Path(*scenario_folder).joinpath(scenario_name)
    print(f'copying {str(src_folder)} to {str(destination)}')
    shutil.copytree(src_folder, destination)

def setup_project(basefolder):
    ''' creates an empty project base folder '''
    required_folders = ['active','archived']
    for required_folder in required_folders:
        folder = basefolder.joinpath(required_folder)
        folder.mkdir(parents=True, exist_ok=True)

@pytest.mark.dependency(name='init')
@pytest.mark.parametrize('test_input,test_output',[
    ('test', 'TEST')
])
def test_project_init_newproject(tmpdir,test_input,test_output):
    ''' tests project init '''
    # test setup
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    expresult = [test_output, basefolder.joinpath(test_output)]

    # test execution
    project = Project(str(basefolder), test_input)
    actresult = [project.name, project.path]

    # test verification
    assert actresult[0] == expresult[0]  # project name
    assert actresult[1] == expresult[1]  # project path


@pytest.mark.dependency(depends=['init'])
@pytest.mark.parametrize('test_input,test_output',[
    (['a short message'],[r'a short message']),
    (['message 1', 'message 2'], [r'message 1', r'message 2']),
    (['message 1\n\nmessage 2'], [r'message 1\n\nmessage 2']),
])
def test_add_note(tmpdir, test_input, test_output):
    ''' check notes can be added '''
    # test setup
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER, 'test')
    entrytime = datetime.utcnow()
    entrytime_str = entrytime.strftime('%Y-%m-%dT%H:%MZ')
    expresult = r''

    for entry in test_output:
        expresult = expresult + r"\n\n## \d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}Z"
        expresult = expresult + r'\n\n' + entry

    # test execution
    project = Project(str(basefolder.parent), 'test')
    project.create()

    for entry in test_input:
        project.add_note(entry, entrytime)

    with open(basefolder.joinpath(project.notes_file), 'r') as fh1:
        notes_file = fh1.read()

    actresult = re.search(expresult, notes_file)
    print(expresult)
    print(notes_file)

    # test verification
    assert actresult is not None

@pytest.mark.dependency(name='create', depends=['init'])
def test_create(tmpdir, caplog):
    ''' checks a project can be created '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')

    assert projectfolder.exists

@pytest.mark.dependency(name='create_exist', depends=['init'])
def test_create_exists(tmpdir, caplog):
    ''' checks a project isn't created if the project folder already exists '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')
    pr.create()

    with pytest.raises(ProjectExistsError):
        pr.create()

@pytest.mark.dependency(name='create_abstract', depends=['init', 'create'])
def test_create_abstract(tmpdir, caplog):
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')
    pr.create(abstract='an abstract')

    expresult = 'an abstract'

    with open(pr.notes_file, 'r') as fh1:
        notestr = fh1.read()

    actresult = re.search('an abstract', notestr).group(0)

    assert actresult == expresult

def test_todotxt_file(tmpdir):
    ''' checks todotxt file is as expected '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)

    pr = Project(str(basefolder), 'test')

    expresult = basefolder.joinpath('test'.upper(),'todo.txt')
    actresult = pr.todotxt_file

    assert actresult == expresult

@pytest.mark.dependency(name='task_add_simple', depends=['init'])
@pytest.mark.parametrize('test_input, test_output', [
    (['test 1'], ['test 1']),
    (['test 1', 'test 2'], ['test 1', 'test 2']),
    ])
def test_task_add_simple(tmpdir, caplog, test_input, test_output):
    ''' checks adding todotxt when todo.txt doesn't exist '''
    caplog.set_level(logging.DEBUG)
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')
    pr.create()

    for task in test_input:
        pr.task_add(description=task)

    expresult = "\n".join(test_output) + "\n"

    with open(pr.todotxt_file, 'r') as fh1:
        actresult = fh1.read()

    assert actresult == expresult

@pytest.mark.dependency(name='task_list_simple', depends=['task_add_simple'])
@pytest.mark.parametrize('test_input, test_output', [
    (['test 1'], ['test 1']),
    (['test 1', 'test 2'], ['test 1', 'test 2']),
    (['test 1', 'x test 2'], ['test 1', 'test 2']),
    ])
def test_task_list_all(tmpdir, caplog, test_input, test_output):
    ''' checks the task list function '''
    caplog.set_level(logging.DEBUG)
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')
    pr.create()

    for task in test_input:
        pr.task_add(description=task)

    tasks = pr.task_list()

    actresult = []
    for task in tasks:
        actresult.append(task.description)

    expresult = test_output

    assert actresult == expresult

@pytest.mark.dependency(name='task_list_simple', depends=['task_add_simple'])
@pytest.mark.parametrize('test_input, test_output', [
    (['test 1'], ['test 1']),
    (['test 1', 'test 2'], ['test 1', 'test 2']),
    (['test 1', 'x test 2'], ['test 1']),
    ])
def test_task_list_not_completed(tmpdir, caplog, test_input, test_output):
    ''' checks the task list function '''
    caplog.set_level(logging.DEBUG)
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')
    pr.create()

    for task in test_input:
        pr.task_add(description=task)

    tasks = pr.task_list(completed=False)

    actresult = []
    for task in tasks:
        actresult.append(task.description)

    expresult = test_output

    assert actresult == expresult

@pytest.mark.dependency(name='task_list_simple', depends=['task_add_simple'])
@pytest.mark.parametrize('test_input, test_output', [
    (['test 1'], []),
    (['test 1', 'test 2'], []),
    (['test 1', 'x test 2'], ['test 2']),
    ])
def test_task_list_completed(tmpdir, caplog, test_input, test_output):
    ''' checks the task list function '''
    caplog.set_level(logging.DEBUG)
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    projectfolder = basefolder.joinpath('test')

    pr = Project(str(basefolder), 'test')
    pr.create()

    for task in test_input:
        pr.task_add(description=task)

    tasks = pr.task_list(completed=True)

    actresult = []
    for task in tasks:
        actresult.append(task.description)

    expresult = test_output

    assert actresult == expresult

@pytest.mark.parametrize('test_input, test_project', [
    ('simple', 'SINGLE')
])
def test__archive_project(tmpdir, test_input, test_project):
    ''' checks projects can be archived properly '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    copyscenario(test_input, basefolder)
    projectfolder = basefolder.joinpath('active', test_project.upper())
    archivefolder = basefolder.joinpath('archived')
    archivefile = archivefolder.joinpath(test_project.upper() + '.zip')

    pr = Project(str(basefolder.joinpath('active')),
        test_project.upper())

    pr._archive_project(archive_folder=archivefolder)

    # archive should include all files not under the 'tmp' folder
    expresult = []
    filelist = projectfolder.glob('**/*')
    for thisfile in filelist:
        if thisfile.is_file():
            short_path = thisfile.relative_to(basefolder.joinpath('active'))
            print(f'checking {str(thisfile)} for archivability')
            archivable_file = True
            for thispath in short_path.parents:
                if thispath.name == 'tmp':
                    archivable_file = False
                    break
            if archivable_file:
                print(f'adding {str(short_path)} to expresult')
                expresult.append(short_path)

    with zipfile.ZipFile(archivefile, 'r') as fh1:
        actresult = [Path(x) for x in fh1.namelist()]

    assert sorted(actresult) == sorted(expresult)


@pytest.mark.parametrize('test_input, test_project', [
    ('simple', 'SINGLE')
])
def test__remove_project(tmpdir, test_input, test_project):
    ''' checks projects can be removed properly '''
    basefolder = Path(tmpdir).joinpath(PROJECT_FOLDER)
    copyscenario(test_input, basefolder)
    projectfolder = basefolder.joinpath('active', test_project.upper())


    pr = Project(str(basefolder.joinpath('active')), test_project.upper())

    pr._remove_project()

    assert not projectfolder.exists() 
