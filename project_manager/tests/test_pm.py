from pathlib import Path
import pytest
import pytest_mock

from ..pm import create_project as create_project
from ..project import Project as Project

def setup_project(basefolder):
    ''' creates an empty project base folder '''
    required_folders = ['active','archived']
    for required_folder in required_folders:
        folder = basefolder.joinpath(required_folder)
        folder.mkdir(parents=True, exist_ok=True)

@pytest.mark.xfail(reason='Mocking not working ')
@pytest.mark.parametrize('test_input, test_output', [
    ({'project': 'test'}, {'project': 'test'})
])
def test_create_project(tmpdir, mocker, test_input, test_output):
    basefolder = Path(tmpdir).joinpath('PROJECTS')
    setup_project(basefolder)

    mockedclass = mocker.patch('project_manager.project.Project')

    create_project(**test_input, basefolder=str(basefolder))

    assert False
    #assert mockedclass.assert_called_once()
