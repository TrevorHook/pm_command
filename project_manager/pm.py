''' Provides a CLI interface to the project manager '''

# standard library imports
import argparse
import configparser
import logging
from pathlib import Path

# local package imports
from project_manager.project import ProjectManager, Project
from project_manager.log import setup_log

PROJECT_BASEFOLDER = Path().home().joinpath('projects')
PROJECT_CONF = ProjectManager.config_file()
LOGFILE = Path().home().joinpath('logs', 'projectmanager.log')
TEXT_EDITOR = r'C:\Users\trevor.hook\AppData\Local\atom\atom.exe'
PROJECT_MANAGER = None

def show_projects(**kwargs):
    ''' shows the project list

    ARGUMENTS:
    kwargs['project_list'] -- a list of project objects
                              if the key doesn't exist a default list of
                              all open projects is used.
    '''

    logging.info('list active projects')

    try:
        project_list = kwargs['project_list']
    except KeyError:
        project_manager = ProjectManager()
        project_list = project_manager.projects_open()


    line_format = "{:^6}  {:>5}  {:>7}  {}"
    status_lines = [
        ['Active', 'Tasks', 'Time(m)', 'Project'],
        ['------', '-----', '-------', '------------------']]

    # produce a status line for each project
    for project in project_list:

        task_list = project.task_list(completed=False)
        task_list_count = len(task_list)

        # populate a dict with elements of each project line
        status_line = []
        if project.active:
            status_line.append('A')
        else:
            status_line.append('')

        status_line.append(str(task_list_count).rjust(3, ' '))
        status_line.append(int(project.active_duration / 60))
        status_line.append(project.name)

        status_lines.append(status_line)

    # print out project report
    for this_line in status_lines:
        print(line_format.format(*this_line))

def show_tasks(task_list):
    ''' show a numbered task list '''
    linenr = 1

    # tasks should be sorted based on priority
    task_list = sort_tasks(task_list)

    description_lenght = [len(x.description) for x in task_list]

    if len(task_list) > 0:
        for task in task_list:
            if task.priority is None:
                priority = ''
            else:
                priority = task.priority

            print('{:>3}  {:<1}  {:<{desc_width}}  {}'.format(
                linenr, priority, task.description, task.project,
                desc_width=max(description_lenght)))
            linenr += 1
    else:
        print("No Uncompleted Tasks !!!")

def sort_tasks(task_list):
    ''' returns the task list sorted by priority '''
    return sorted(
        task_list,
        key = lambda x: 'Z' if x.priority is None else x.priority)

def close_projects(**kwargs):
    ''' closes 1 or more projects '''
    project_manager = ProjectManager()

    # loop through each project and close it
    for project in kwargs['projects']:
        project_manager.close(project)
    show_projects()

def create_project(**kwargs):
    ''' creates a new project '''
    project_manager = ProjectManager()

    abstract = kwargs.get('abstract', None)

    project = Project(
        project_manager.active_folder,
        kwargs['project'],
        **kwargs)

    project.create(abstract=abstract)
    show_projects()

def add_note(**kwargs):
    ''' adds a note to a project '''
    project_manager = ProjectManager()

    project = project_manager.project(kwargs['project'])
    project.add_note(kwargs['note'])
    show_projects()

def start(**kwargs):
    ''' adds a note to a project '''
    project_manager = ProjectManager()

    project_manager.start(kwargs['project'])
    show_projects()

def stop(**kwargs):
    ''' adds a note to a project '''
    project_manager = ProjectManager()

    project_manager.stop(kwargs['project'])
    show_projects()

def add_log(**kwargs):
    ''' add a log message to the work journal '''
    project_manager = ProjectManager()

    project_manager.workjournal_entry(kwargs['note'])
    show_projects()

def tasks(**kwargs):
    ''' displays all uncompleted tasks in open projects '''
    project_manager = ProjectManager()

    task_list = project_manager.active_tasks(
        project=kwargs['project'])
    show_tasks(task_list)

def add_tasks(**kwargs):
    ''' adds a set of tasks to a named project '''
    project_manager = ProjectManager()

    this_project = project_manager.project(kwargs['project'])
    for thistask in kwargs['tasks']:
        this_project.task_add(description=thistask)
    show_projects()

def setup_cli():
    ''' setups and returns the argparse arguments '''
    # setup Argument_Parser
    parser = argparse.ArgumentParser()
    parser.set_defaults(action='list')
    parser.add_argument('--debug', action='store_true',
            help='turn on debug logging')

    subparser = parser.add_subparsers()

    # list projects
    sub_list = subparser.add_parser('list')
    sub_list.set_defaults(action='list')

    # create project
    sub_create = subparser.add_parser('create')
    sub_create.set_defaults(action='create')
    sub_create.add_argument('project')
    sub_create.add_argument('-a', '--abstract', help="A short project intro")
    sub_create.add_argument('-d', '--date', action='store_true',
        help='Append the date to the project name')

    # start working on a project
    sub_start = subparser.add_parser('start')
    sub_start.set_defaults(action='start')
    sub_start.add_argument('project')

    # stop working on a project
    sub_stop = subparser.add_parser('stop')
    sub_stop.set_defaults(action='stop')
    sub_stop.add_argument('project', nargs='?')

    # close project
    sub_close = subparser.add_parser('close')
    sub_close.set_defaults(action='close')
    sub_close.add_argument('projects', nargs='+')

    # project note
    sub_note = subparser.add_parser('note')
    sub_note.set_defaults(action='note')
    sub_note.add_argument('project')
    sub_note.add_argument('note', help="A short project note")

    # project add task
    sub_addtask = subparser.add_parser('add_tasks')
    sub_addtask.set_defaults(action='addtask')
    sub_addtask.add_argument('project')
    sub_addtask.add_argument('tasks', nargs='+',
        help="task to add to the project")

    # project show task
    sub_showtask = subparser.add_parser('show_tasks')
    sub_showtask.set_defaults(action='showtask')
    sub_showtask.add_argument('project', nargs='?', default=None)

    # workjournal log
    sub_log = subparser.add_parser('log')
    sub_log.set_defaults(action='log')
    sub_log.add_argument('note', help="A short workjournal note")

    return parser.parse_args()

def main():
    ''' setups the cli and calls supporting functions '''

    args = setup_cli()

    logger = setup_log('projectmanager.log')
    if args.debug:
        logger.setLevel(logging.DEBUG)

    # projectconfig
    config = configparser.ConfigParser()
    if not PROJECT_CONF.exists():
        logger.warning(
            "projectmanager.ini not found, creating with default values")
        config['LOCATIONS'] = {}
        config['LOCATIONS']['PROJECT_BASEFOLDER'] = str(PROJECT_BASEFOLDER)
        with open(PROJECT_CONF, 'w') as configfile:
            config.write(configfile)

    function_map = {
        'create': create_project,
        'close': close_projects,
        'list': show_projects,
        'note': add_note,
        'start': start,
        'stop': stop,
        'log': add_log,
        'showtask': tasks,
        'addtask': add_tasks}

    try:
        function_map[args.action](**vars(args))
    except KeyError:
        logging.warning('invalid function_map key used, no action taken')

if __name__ == '__main__':
    main()
