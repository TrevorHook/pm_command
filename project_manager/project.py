''' a script to manage project workflows '''

# standard library imports
import configparser
from datetime import datetime
import json
import logging
import os
from pathlib import Path
import time
import zipfile

# 3rd party imports
import pytodotxt


class ProjectExistsError(Exception):
    ''' Indicated a project already exists when it was not expected to '''

DEFAULT_BASEFOLDER = Path().home().joinpath('project')

class ProjectManager():
    ''' manages lists of projects '''

    def __init__(self, **kwargs):
        ''' initialize project manager '''
        self.projects = set()
        self.log = []

        # if a config file location is provided in kwargs, use it, if not use
        # the default
        config_file = kwargs.get('config_file', None)
        if config_file:
            config_file = Path(config_file)
        else:
            config_file = self.config_file()

        self.config = self._read_config(config_file)

        # override locations if provied by kwargs
        basefolder = kwargs.get('basefolder', None)
        active = kwargs.get('active', None)
        archived =  kwargs.get('archived', None)

        if basefolder is not None:
            self.config['LOCATIONS']['basefolder'] = basefolder

        if active is not None:
            self.config['LOCATIONS']['active'] = active

        if archived is not None:
            self.config['LOCATIONS']['archived'] = archived

        self.projects.update(self._find_open_projects())
        self._read_log()

    ## CLASSMETHODS
    #
    #

    @classmethod
    def config_file(cls):
        ''' returns the expected location of the config file.

        This is a classmethod so you don't have to instatiate the class to
        get this information
        '''

        filepath = Path().home().joinpath('.projectmanager.ini')
        logging.debug('config_file = %s', str(filepath))
        return filepath

    ## PROPERTIES
    #
    #

    @property
    def basefolder(self):
        ''' returns the basefolder '''
        return Path(self.config['LOCATIONS']['basefolder'])

    @property
    def active_folder(self):
        ''' returns the folder that active projects are kept in '''
        return self.basefolder.joinpath(self.config['LOCATIONS']['active'])

    @property
    def archived_folder(self):
        ''' returns the folder that archives are kept in '''
        return self.basefolder.joinpath(self.config['LOCATIONS']['archived'])

    @property
    def workjournal_file(self):
        ''' returns the location of the work journal file '''
        return self.basefolder.joinpath('work journal.md')

    @property
    def log_file(self):
        ''' returns the location of the log file '''
        return self.basefolder.joinpath('log.json')

    ## PUBLIC FUNCTIONS
    #
    #

    def create(self, name, **kwargs):
        ''' creates a new active project '''

        # create standard format name
        name = name.replace(' ', '_').upper()
        logging.info('creating new project: %s', name)

        if kwargs.get('date', False):
            date = datetime.now().strftime('%Y%m%d')
            name = name + "." + date

        newproject = Project(self.active_folder, name)

        try:
            newproject.create(**kwargs)
        except ProjectExistsError as this_error:
            logging.warning('Attempted to create %s but it already exists',
                name)
            raise this_error

        self.projects.add(newproject)
        self._log_event(event="Created", project=name)
        self.workjournal_entry(f"Created {name}")

    def close(self, project_names):
        ''' archives a project

        ARGUMENTS:
        name -- a list of project names
        '''
        if not isinstance(project_names, list):
            project_names = [project_names]

        for project_name in project_names:
            project = self.project(project_name)
            project.close(archive_folder=str(self.archived_folder))

            self._log_event(event="Closed", project=project_name)
            self.workjournal_entry(f'Closed {project_name}')

    def start(self, name):
        ''' indicate a project is being actively worked on '''
        logging.info('starting %s', name)

        # we assume only 1 project can be worked on at once
        self.stop()

        # all projects should be stopped, start the project requested
        project = self.project(name)
        project.start()
        self._log_event(event='Start', project=name)
        self.workjournal_entry(f'Working on {name}')

    def stop(self, project_name=None):
        ''' indicate a project is not being actively worked on '''
        if project_name is None:
            logging.info('stopping all projects')
            for project in self._find_open_projects():
                if project.stop():
                    self.workjournal_entry(f'Stopping work on {project.name}')
                    self._log_event(event='Stop', project=project.name)
        else:
            logging.info('stopping %s', project_name)
            self.project(project_name).stop()

    def list(self):
        ''' lists active projects '''
        active_paths = self._find_open_projects()

        active_projects = {}
        for project in active_paths:
            active_projects[project.name] = {'path': project.path}

        return active_projects

    def projects_open(self):
        ''' returns a list of project objects '''
        return self._find_open_projects()

    def active_tasks(self, project=None):
        '''
        returns a list of active tasks

        ARGUMENTS:
        project -- If None, finds open tasks from all projects
                   If not None, finds open tasks from the Named project
        '''

        task_list = []
        if project is None:
            for this_project in self.projects_open():
                task_list.extend(this_project.active_tasks)
        else:
            task_list.extend(self.project(project).active_tasks)

        return task_list

    def project(self, name):
        ''' returns an active project based on name '''

        # TODO: insert check for project existing
        return Project(self.active_folder, name)

    def projectlog_entry(self, project, message, entrytime=datetime.utcnow()):
        ''' writes a short message to a project org file '''
        project = self.project(project)
        project.add_note(message, entrytime)

    def workjournal_entry(self, message, entrytime=datetime.utcnow()):
        ''' writes a short message to the work journal file '''
        logging.debug('creating workjournal %s at %s', message, str(entrytime))
        now = entrytime.strftime('%Y-%m-%dT%H:%MZ')
        note = [f'\n## {now}','']
        for line in message.split(r'\n'):
            note.append(line)

        with open(self.workjournal_file, 'a') as fh1:
            for line in note:
                fh1.write(f'\n{line}')

    ## PROTECTED FUNCTIONS
    #
    #

    def _read_config(self, config_file_path=None):
        ''' reads the config ini file for this user '''
        config = configparser.ConfigParser()

        if config_file_path is None:
            config_file_path = self.config_file()

        if config_file_path.exists():
            config.read(config_file_path)
        else:
            config.read_dict({'LOCATIONS': {
                'basefolder': DEFAULT_BASEFOLDER,
                'active': 'active',
                'archived': 'archived'}
            })
        return config

    def _find_projects(self):
        ''' returns a list of project paths '''
        paths = []
        paths.extend(self._find_open_projects())
        paths.extend(self._find_archived_projects())


    def _find_open_projects(self):
        ''' returns a list of active project paths '''
        active_projects = []
        for child in self.active_folder.iterdir():
            if child.is_dir():
                try:
                    project = Project(self.active_folder, child.name)
                    active_projects.append(project)
                except PermissionError:
                    # unable to read folder, normally due to OneDrive sync
                    logging.warning('unable to read %s', child.name)


        return active_projects

    def _find_archived_projects(self):
        ''' returns a list of active project paths '''
        archived_projects = []
        for child in self.archived_folder.iterdir():
            if child.glob('*.zip'):
                archived_projects.append(child)

        return archived_projects

    def _read_log(self):
        ''' reads the event log to self.log '''
        try:
            with open(self.log_file, 'r') as fh1:
                self.log = json.load(fh1)
        except FileNotFoundError:
            # log file not found, self.log will be empty
            pass

    def _write_log(self):
        ''' writes the event log to file '''
        with open(self.log_file, 'w') as fh1:
            json.dump(self.log, fh1, indent=4)

    def _log_event(self, **kwargs):
        ''' adds an event to the project manager log '''
        eventtime = datetime.timestamp(datetime.utcnow())
        log = {'eventtime':eventtime}
        for key, value in kwargs.items():
            log[key] = value
        self.log.append(log)
        self._write_log()


class Project():
    ''' represents a single project '''

    STATE_SCHEMA_VERSION = 1

    def __init__(self, basefolder, name, **kwargs):
        ''' initialize project from a project folder '''

        if isinstance(basefolder, str):
            basefolder = Path(basefolder)

        append_date = kwargs.get('date', False)
        if append_date:
            name = name + '.' + self._datenow()

        name = name.replace(' ', '_')

        name = name.upper()

        self.details = {}
        self.details['path'] = basefolder.joinpath(name)
        self.details['name'] = name

        logging.debug('initialization project at %s',
            self.details['path'])

        # setup a default state dict
        self.state = {
            'active': None,
            'active_duration_seconds': 0,
            'active_starttime': None,
            'project_name': self.path.name,
            'logs': [],
            'state_schema': 1}
        # populate state dict with stored state if possible
        self._read_state()

    @property
    def archive_file(self):
        ''' returns the archive file '''
        archivename = self.name.split('.')[0]
        return archivename + '.zip'

    @property
    def active_tasks(self):
        ''' returns a list of active tasks associated with the project '''
        return self.task_list(completed=False)

    @property
    def path(self):
        ''' return the project path '''
        return self.details['path']

    @property
    def name(self):
        ''' return the project name '''
        return self.path.name.upper()

    @property
    def notes_file(self):
        ''' returns the file path for the notes file '''
        return self.path.joinpath('notes.md')

    @property
    def todotxt_file(self):
        ''' returns the path of the todotxt file '''
        return self.path.joinpath('todo.txt')

    @property
    def statefile(self):
        ''' returns the path to the state file '''
        return self.path.joinpath('state.json')

    @property
    def active(self):
        ''' returns a boolean for whether the project is active '''
        logging.debug('%s active = %s', self.name, str(self.state['active']))
        return self.state['active']

    @property
    def age(self):
        ''' returns the age of the project in seconds, or None if the age
        can't be calculated '''

        end_time = Project._timenow()
        start_time = self.state['created_starttime']
        return Project._seconds_from_string(end_time, start_time)

    @property
    def active_duration(self):
        '''' return the active duration of the project in seconds '''
        duration = self.state['active_duration_seconds']
        if self.active:
            duration += Project._seconds_from_string(
                self.state['active_starttime'],
                Project._timenow())
        return duration

    ##### PUBLIC FUNCTIONS
    #
    #

    def create(self, **kwargs):
        ''' create a project '''
        if self.path.exists():
            raise ProjectExistsError('%s already exists' % self.path)

        self._save_project()
        self._create_notes_file(**kwargs)
        self._todotxt_create()
        self._log_event(event='Created', project=self.name)

    def close(self, archive_folder=None):
        ''' close a project '''
        self.stop()
        self._log_event(event='Closed', project=self.name)
        self.add_note("Project closed")
        archive_file = self._archive_project(archive_folder=archive_folder)
        self._remove_project()
        return archive_file

    def start(self):
        ''' start working on a project '''
        self._log_event(event='Start')
        self._set_state(
            active=True,
            active_starttime= Project._timenow(),
            active_duration_seconds=self._active_duration(self.state['logs'])
            )

    def stop(self):
        ''' start working on a project '''
        if self.active:

            # calculate duration of last active period
            duration_sec = Project._seconds_from_string(
                self.state['active_starttime'],
                Project._timenow())

            self._log_event(event='Stop', duration=duration_sec)

            total_duration = (self.state['active_duration_seconds'] +
                duration_sec)

            self._set_state(
                active=False,
                active_duration_seconds=total_duration,
                active_starttime=None
            )

            logging.info('stopping %s', self.name)

    def add_note(self, notestr, entrytime=datetime.utcnow()):
        ''' adds a new note to the notes.org file for the project '''
        entrytime_str = entrytime.strftime('%Y-%m-%dT%H:%MZ')
        logging.debug('adding project note %s at %s', notestr, entrytime_str)
        message = ['\n## ' + entrytime_str,'']
        lines = notestr.split(r'\n')

        for line in lines:
            message.append(line)

        self._add_note(message)

    def task_add(self, **kwargs):
        ''' adds a new task to the project '''
        self._task_add(**kwargs)

    def task_list(self, **kwargs):
        ''' lists tasks, kwargs can be used to filter the results.

        see _todotxt for details of the filtering
        '''
        return self._todotxt_list(**kwargs)

    ##### PROTECTED FUNCTIONS
    #
    #

    def _save_project(self):
        ''' saves the project state in it's project folder '''
        logging.debug('creating new project directory at %s', str(self.path))
        self.path.mkdir(parents=True, exist_ok=True)
        self.path.joinpath('tmp').mkdir(parents=True, exist_ok=True)
        self._todotxt_create()

    def _create_notes_file(self, **kwargs):
        ''' creates a new notes file '''

        note_file_name = self.notes_file.name

        if self.notes_file.exists():
            logging.warning('%s already exists', str(self.notes_file))
            raise FileExistsError('{} already exists'.format(note_file_name))

        # write out an initial org note file
        with open(self.notes_file, 'w') as fh1:
            fh1.write(f'# {self.name}')

        abstract = kwargs.get('abstract', None)
        if abstract is not None:
            logging.debug('adding abstract to notes.org')
            note_list = ['\n']
            note_list.extend(abstract.split('\n'))
            self._add_note(note_list)

    def _add_note(self, note_list):
        ''' appends a note to the notes.org file '''
        logging.debug('adding note %s', str(note_list))
        with open(self.notes_file, 'a') as fh1:
            for line in note_list:
                fh1.write('\n{}'.format(line))

    def _task_add(self, **kwargs):
        ''' adds a task to the project task file '''
        logging.debug('adding task %s to %s', kwargs['description'], self.name)

        # Setup Task
        task = pytodotxt.Task(kwargs['description'])

        # Add tast to todotxt file
        todotxt =  self._todotxt()
        todotxt.tasks.append(task)
        todotxt.save()

    def _todotxt(self):
        ''' returns  a todotask object '''
        if not self.todotxt_file.exists():
            self._todotxt_create()

        todotxt = pytodotxt.TodoTxt(self.todotxt_file)
        todotxt.parse()

        return todotxt

    def _todotxt_create(self):
        ''' creates the todotxt file if it doesn't exist '''
        if not self.todotxt_file.exists():
            logging.debug('todotxt is being created')
            open(self.todotxt_file, 'a').close()

    def _todotxt_list(self, **kwargs):
        ''' returns a list task

        ARGUMENTS:
        kwargs['completed']='True' -- returns only completed tasks
        kwargs['completed']='False' -- returns only incomplete tasks
        '''
        todotxt = self._todotxt()
        tasks = []

        complete_st = kwargs.get('completed', None)

        for task in todotxt.tasks:
            conditions_match = True

            # check is task matches completed state
            if complete_st is not None and complete_st != task.is_completed:
                conditions_match = False

            # set the project to the project name that contains the todo file
            if conditions_match:
                task.project = self.name

            # if conditions_match == True, include in return list
            if conditions_match:
                tasks.append(task)

        return tasks

    def _read_state(self):
        ''' reads the state file into self._state '''
        try:
            # open the statefile
            with open(self.statefile, 'r') as fh1:
                state = json.load(fh1)

            # repopulate self.state with just the keys found in statefile
            # this allows new state keys to be added in the future easily
            for key, value in state.items():
                self.state[key] = value
        except FileNotFoundError:
            # state file has not been found, use the a default state dict
            logging.warning('unable to read %s, using defaults', self.statefile)

    def _write_state(self):
        ''' write the self.state to the state file '''
        with open(self.statefile, 'w') as fh1:
            json.dump(self.state, fh1, indent=4)

    def _set_state(self, **kwargs):
        ''' sets the state of various fields to values and saves the changes '''
        for key, value in kwargs.items():
            self.state[key] = value

        self._write_state()

    def _log_event(self, **kwargs):
        ''' log an event '''
        eventtime = Project._timenow()
        logentry = {'eventtime': eventtime}
        for key, value in kwargs.items():
            logentry[key] = value

        logging.debug('logging event: %s', str(logentry))
        self.state['logs'].append(logentry)

        self._write_state()

    def _archive_project(self, archive_folder=None):
        ''' archives the project to the archive file '''

        # work out where to place the archive file.
        if archive_folder is not None:
            archive_file = Path(archive_folder).joinpath(self.archive_file)
        else:
            archive_file = self.path.parent.joinpath(self.archive_file)

        logging.info('archiving project to %s', str(archive_file))

        # build the set of files to archive
        allfiles = set(self.path.rglob(r'*.*'))
        excludedfiles = set(self.path.rglob(r'tmp/**/*'))
        includedfiles = list(allfiles - excludedfiles)

        # archive the files.
        with zipfile.ZipFile(archive_file,
                'a', zipfile.ZIP_DEFLATED) as zip_file:

            for thisfile in includedfiles:
                relativepath = thisfile.relative_to(self.path.parent)
                logging.debug('zipping %s', relativepath)
                zip_file.write(thisfile, relativepath)

        # return the file the archive was written to
        return archive_file

    def _remove_project(self):
        ''' remove the project files '''
        logging.info('removing project files')

        counter = 20
        while self.path.exists() and counter > 0:
            for root, dirs, files in os.walk(self.path, topdown=False):

                try:
                    # remove files
                    for this_file in files:
                        path = os.path.join(root, this_file)
                        os.unlink(os.path.join(root, this_file))

                    # remore directories
                    for this_dir in dirs:
                        path = os.path.join(root, this_dir)
                        os.rmdir(path)

                    # remove root directory
                    os.rmdir(root)
                    break

                except PermissionError:
                    logging.warning('PermissionError on %s', str(path))
                except OSError:
                    logging.warning('OSError on %s', str(path))
            counter -= 1


    ##### STATIC METHODS
    #
    #
    @staticmethod
    def _timenow(time_to_convert=datetime.utcnow()):
        ''' returns the current time as a formated string '''
        return time_to_convert.strftime('%Y-%m-%dT%H:%M:%SZ')

    @staticmethod
    def _datenow(date_to_convert=datetime.utcnow()):
        ''' returns the current date as a formated string '''
        return date_to_convert.strftime('%Y%m%d')

    @staticmethod
    def _seconds_from_string(starttime, endtime):
        ''' takes 2 string representations of a datetime, as found in our
        statefile, and returns the number of seconds between them '''

        starttime = datetime.strptime(starttime, '%Y-%m-%dT%H:%M:%SZ')
        endtime = datetime.strptime(endtime, '%Y-%m-%dT%H:%M:%SZ')
        duration = endtime - starttime
        return  duration.days * 24 * 3600 + duration.seconds

    @staticmethod
    def _active_duration(statelog):
        '''
        returns the total active duration for the project in seconds,
        based on a sum of the durations listed for Stop events in
        the state dictionary.

        ARGUMENTS:
        statelog -- a list of dictionaries
                    {event = "Stop", duration=durationinseconds }
        '''

        durations = 0
        for eventlog in statelog:
            try:
                if eventlog['event'] == 'Stop':
                    durations += eventlog['duration']
            except KeyError:
                # either th event or duration fields were missing, ignore
                # this record and move to the next
                pass

        return durations
