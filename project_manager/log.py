''' setup logging '''

import logging
import logging.config
import os

LOG_FOLDER = os.path.join(os.path.expanduser('~'),'logs')
LOG_CONFIG_FOLDER = os.path.split(os.path.abspath(__file__))[0]
LOG_INI = os.path.join(LOG_CONFIG_FOLDER,'logging.ini')

def setup_log(logfile, debug=False, logfolder=LOG_FOLDER, debugenv='LOG_DEBUG'):
    ''' setups the logging system '''

    if not os.path.exists(logfolder):
        os.mkdir(logfolder)

    if not os.path.exists(LOG_INI):
        logging.warning('%s not found', LOG_INI)

    logpath = os.path.join(logfolder, logfile)

    # setup logging
    logging.config.fileConfig(
        LOG_INI,
        defaults={
            'logfilename': str(logpath).encode('unicode-escape').decode(),
            'rotatewhen': 'W0',
            'rotateinterval': 0,
            'backupcount': 4})

    logger = logging.getLogger()

    if os.getenv(debugenv, None) is None:
        debug = False
    else:
        debug = True

    if debug:
        logger.setLevel(logging.DEBUG)
        logging.info('debug information will be sent to %s', logpath)

    return logger
